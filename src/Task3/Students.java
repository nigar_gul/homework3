package Task3;

public class Students {
    private String name;
    private int age;
    private double grade;


    public void setName(String name) {
        if (name.isEmpty()) {
            System.out.println("Name can't be empty");
        } else {
            this.name = name;
        }
    }
    public String getName() {
        return name;
    }

    public void setAge(int age) {
        if (age < 0) {
            System.out.println("Age can't be negative");
        } else {
            this.age = age;
        }
    }
    public int getAge() {
        return age;
    }


    public void setGrade(double grade) {
        if (grade >= 91) {
            this.grade=grade;
            System.out.println("I'm an excellent student");
        } else if (grade >= 71) {
            this.grade=grade;
            System.out.println("I'm a high achiever student");
        } else if (grade >= 51) {
            this.grade=grade;
            System.out.println("I'm a good student");
        } else {
            this.grade=grade;
            System.out.println("I need an improvement");
        }
    }
    public double getGrade() {
        return grade;
    }
}

