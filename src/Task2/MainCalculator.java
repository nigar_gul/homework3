package Task2;

import Task2.Calculator;

import java.util.Scanner;

public class MainCalculator {
    public static void main(String[] args) {
        Calculator calculator=new Calculator ();
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter 1st number");
        calculator.a= scanner.nextDouble();
        System.out.println("Please enter 2nd number");
        calculator.b= scanner.nextDouble();
        System.out.println("Please enter operation");
        calculator.operation= scanner.nextInt();
        calculator.result=0;
        if (calculator.operation==1) {
            calculator.addition();
        } else if (calculator.operation==2) {
            calculator.subtraction();
        } else if (calculator.operation==3) {
            calculator.multiplication();
        } else if (calculator.operation==4) {
            calculator.division();
        } else {
            calculator.invalid();
        }
    }
}


