package Task1;

public class Car extends Vehicle {
    public Car (String colour,int year,String brand) {
        super(colour, year, brand);
    }
    @Override
    public void startEngine() {
        System.out.println("Its engine starts fastly ");
    }
    @Override
    public void drive() {
        System.out.println("It is easy to drive");
    }
}

