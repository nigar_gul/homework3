package Task1;

public class Motorcycle extends Vehicle {
    public Motorcycle (String colour,int year,String brand) {
        super(colour, year, brand);
    }
    @Override
    public void startEngine()
    {
        System.out.println("Its engine starts immediately");
    }
    @Override
    public void drive() {
        System.out.println("It is not easy to drive");
    }
}
