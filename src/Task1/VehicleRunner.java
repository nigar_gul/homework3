package Task1;

public class VehicleRunner {
    public static void main(String[] args) {
        Car car= new Car("red",2023,"Porsche");
        System.out.println("My car's brand is " +car.brand+ ".Its colour is " +car.colour+ " and it's year is " +car.year);
        car.drive();
        car.startEngine();
        Truck truck=new Truck ("blue",2015,"Ford");
        System.out.println("My truck's's brand is " +truck.brand+ ". Its colour is " +truck.colour+ " and it's year is " +truck.year);
        truck.drive();
        truck.startEngine();
        Motorcycle motorcycle=new Motorcycle("black",2021,"Harley Davidson");
        System.out.println("My Motorcycle's brand is " +motorcycle.brand+ ". Its colour is " +motorcycle.colour+ " and it's year is " +motorcycle.year);
        motorcycle.drive();
        motorcycle.startEngine();

    }
}
