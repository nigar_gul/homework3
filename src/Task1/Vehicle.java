package Task1;

public abstract class Vehicle {
    public String colour;
    public int year;
    public String brand;

    public Vehicle(String colour, int year, String brand) {
        this.colour = colour;
        this.year = year;
        this.brand = brand;
    }
    public abstract void startEngine();

    public abstract void drive();
}
