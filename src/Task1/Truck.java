package Task1;

public  class Truck extends Vehicle {
    public Truck (String colour,int year,String brand) {

        super(colour, year, brand);
    }
    @Override
    public void startEngine()
    {
        System.out.println("Its engine starts slowly ");
    }
    @Override
    public void drive() {
        System.out.println("It is not easy to drive");
    }
}
